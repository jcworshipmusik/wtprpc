# CIDM 4390 Tech Info Repo

## Description
As the technical lead this semester, I want to provide a central location for relevant technical information, i.e. links, tutorials, and step-by-step procedures that will help you be successful as you develop your app.


## Table of Contents
1.  AzureDevops
    * [ Remove Side Bar Items ](https://bitbucket.org/jcworshipmusik/wtprpc/src/master/AzureDevOps/RemoveSideBarItems/README.md)

2. [ DotnetSDK ](https://bitbucket.org/jcworshipmusik/wtprpc/src/master/DotnetSDK/README.md)

3. [ Solid Examples](https://bitbucket.org/jcworshipmusik/wtprpc/src/master/SolidExamples/)
    * [Single Responsibility](https://bitbucket.org/jcworshipmusik/wtprpc/src/master/SolidExamples/SingleResponsibility/)
    * [Stairway Pattern](https://bitbucket.org/jcworshipmusik/wtprpc/src/master/SolidExamples/StairwayPattern.md)

4. [ Git Info  ](https://bitbucket.org/jcworshipmusik/wtprpc/src/master/GitInfo/README.md)

5. xUnit Testing
    [ xUnit Testing ](https://bitbucket.org/jcworshipmusik/wtprpc/src/master/XUnitTests/README.md)
    [Unit Testing with xUnit and Moq](https://www.youtube.com/watch?v=kInFI2x7yLY)
    [Intro to Unit Testing in C# using XUnit : IamTimCorey ](https://www.youtube.com/watch?v=ub3P8c87cwk)


6. [Markdown Syntax](https://bitbucket.org/jcworshipmusik/wtprpc/src/master/MarkdownSyntax/README.md)

7. [Identity](https://bitbucket.org/jcworshipmusik/wtprpc/src/master/Identity/README.md)

8. [reCAPTCHA](https://bitbucket.org/jcworshipmusik/wtprpc/src/master/reCAPTCHA/README.md)

9. Bitbucket Info
    * [Git Branching Work Flow](https://bitbucket.org/jcworshipmusik/wtprpc/src/master/Bitbucket/gitworkflow/README.md)
    * [Adding Team Members](https://bitbucket.org/jcworshipmusik/wtprpc/src/master/Bitbucket/addteammembers/README.md)
    * [Git Etiquette](https://bitbucket.org/jcworshipmusik/wtprpc/src/master/Bitbucket/gitetiquette/README.md)

10. Twilio
    * [ Basic Twilio Process ](https://bitbucket.org/jcworshipmusik/wtprpc/src/master/Twilio/Twilio.md)

11. Send Grid
    * [ Send Grid Setup ](https://bitbucket.org/jcworshipmusik/wtprpc/src/master/SendGrid/README.md)

12. Classes
    * [Modeling Real World Objects With Classes](https://bitbucket.org/jcworshipmusik/wtprpc/src/master/ModelingRealWorldObjects/README.md)

13. [Technical Architectural Requirements](https://bitbucket.org/jcworshipmusik/wtprpc/src/master/SolidExamples/architecture.md)

14. Dr. Babb videos
    [Azure Free Tier Plan](https://www.youtube.com/watch?v=-ZVGFjdL-hk)
    [Publishing to Azure using local git](https://www.youtube.com/watch?v=CSi3QBZZEmM)
    [Creating Library Projects and Solutions in Visual Studio Code](https://www.youtube.com/watch?v=0SVf0_s-96E)
    [Working with Linq](https://www.youtube.com/watch?v=kUFsj7KNe-w)

15. [Code Architecture](https://bitbucket.org/jcworshipmusik/wtprpc/src/master/CodeArchitecture/README.md)






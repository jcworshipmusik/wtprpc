## Markdown Syntax
---

The key to creating great looking markdown files, i.e. ` README.md ` or any file with the ` .md ` extention, is know how the write the syntax.  Most of you have some level of knowledge of ` HTML ` and you can use it in conjuction with the markdown syntax.  Markdown syntax is a simpler form of creating headings, unordered lists, ordered lists, etc.  You can read more about how to use this in the link below. 

> This message brought to you by Markdown syntax

---
[Markdown Cheat Sheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Here-Cheatsheet)

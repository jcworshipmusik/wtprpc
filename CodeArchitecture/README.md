# Code Architecture

This diagram shows the basic layout that your project needs to follow.

![Folder Layout](./images/ProjectStructure.jpg)

### MVC Project

As the startup project, you MVC project needs to have references to all other projects.  This is done by adding a reference to those projects. From inside the folder where your <MvcProject>.csproj is located ` dotnet add referenence ../<ClassLib>/<ClassLib>.csproj ` for each project.

### SLN file

This file needs to know about every project that is being used in your overall project.  This is created by navigating to the containing folder and running dotnet new sln.  This file will be named that same name as the containing folder.
The quickest way to add all the projects to your sln file is to run ` dotnet sln add **/*.csproj `  The first 2 ` **  ` represent all folders, and the ` *.csproj ` represents any file with a  ` .csproj ` extension.  With this sln referencing all projects, you can run ` dotnet restore ` or ` dotnet build ` from the containing folder to retore or build all projects with one command.

### ClassLib Project

This project is created by navigating to the containg folder and running  ` dotnet new classlib -o <NameYouChose> `  The creates a new classlib project.  The -o means, create this project and output it into the folder named ` <NameYouChose> ` 

### xUnit Project

This project is created by navigating to the containg folder and running  ` dotnet new xunit -o <NameYouChose> `  The creates a new xUnit project.  The -o means, create this project and output it into the folder named ` <NameYouChose> `.
xUnit needs to reference the project that you will be testing.
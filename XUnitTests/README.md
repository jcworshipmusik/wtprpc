## xUnit testing

This is a great explanation of how to create your xUnit project to test your MVC application. 
* [Testing ASP.NET Core Applications - The Cross Platform Developer Experience](https://www.youtube.com/watch?v=oywjqZX0eWE)

Here's a link to a Microsoft tutorial using xUnit test.
* [Unit testing C# in .NET Core using dotnet test and xUnit](https://docs.microsoft.com/en-us/dotnet/core/testing/unit-testing-with-dotnet-test)

A great teacher on YouTube is Tim Corey.  Here is a link to his xUnit video
* [Intro to Unit Testing in C# using XUnit](https://www.youtube.com/watch?v=ub3P8c87cwk)
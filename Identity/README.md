## ASP.NET CORE Identity

> ASP.NET Core Identity is a membership system that adds login functionality to ASP.NET Core apps. Users can create an account with the login information stored in Identity or they can use an external login provider. Supported external login providers include Facebook, Google, Microsoft Account, and Twitter. - source: https://docs.microsoft.com/en-us/aspnet/core/security/authentication/identity?view=aspnetcore-2.2&tabs=netcore-cli


**caution** 
Always make sure you have a good understanding of what these tools do before you use them into your project.

### Identity Tutorial
- [Introduction to Identity on ASP.NET Core](https://docs.microsoft.com/en-us/aspnet/core/security/authentication/identity?view=aspnetcore-2.2&tabs=visual-studio)

### Scaffolding Identity
- [Scaffold Identity in ASP.NET Core projects](https://docs.microsoft.com/en-us/aspnet/core/security/authentication/scaffold-identity?view=aspnetcore-2.2&tabs=visual-studio)

#### Adding Scaffolding Code Generator to your project
Make sure you're in your main project directory and run the following
```
dotnet add package Microsoft.VisualStudio.Web.CodeGeneration.Design
dotnet restore
```
If the code genenrator doesen't work you may need to install it globally by running the following command:
```
dotnet tool install -g dotnet-aspnet-codegenerator
```



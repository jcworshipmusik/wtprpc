# Sign up for Twilio

1. Set up your free account at [Twilio](https://www.twilio.com)
2. In the dashboard, you will need to copy and save these following items
    * Account SID
    * Auth Token

![Dashboard](./images/ScreenShot1.jpg)

* Get your free phone number

   
# Adding Twilio to your project

1. Navigate to your MVC project folder and run the following command to add the Twilio nuGet package to your project  ` dotnet add package Twilio `.

2. Adding another packages to your project ` dotnet add package Microsoft.Extensions.Configuration `

3. In the appsettings.json file, add the TwillioAccountDetails object in as shown below.
    ** for simplicity you can insert your sid and token directly in for the values.

```json
"ConnectionStrings": {
    "DefaultConnection": "Default Connection Details",
    "ApplicationDbContextConnection": "DataSource=YourProject.db"
  },
  "TwilioAccountDetails": {
    "AccountSid": "Sid you got when you set up your account",
    "AuthToken": "Auth Token you got when you se up your account"
  },
```

# Adding Twilio to your Startup.cs file

1. Add this line above the ` services.AddMVC() ` line

```csharp
 services.Configure<TwilioAccountDetails>(Configuration.GetSection("TwilioAccountDetails"));
```

2. Add a folder Configuration to your project and add TwilioAccountDetails.cs inside it.  Add the following code to it.  Don't forget to add your project's namespace before pasting this code.

```csharp
public class TwilioAccountDetails
{
   public string AccountSid { get; set; }
   public string AuthToken { get; set; }
}
```


# Setting up a test controller to send your first message.

1. Create a MessageController.cs in your Controller folder, add the appropriate namespace for your project,  and copy the following code into your the MessageController.cs.  Remember to change the namespace.



```csharp
using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Twilio;
using Twilio.Rest.Api.V2010.Account;

namespace YOUR_PROJECT_NAMESPACE
{
    public class MessageController : Controller
    {
        public MessageController(IOptions<TwilioAccountDetails> twilioAccountDetails)
        {
            _twilioAccountDetails = twilioAccountDetails.Value ?? throw new ArgumentException(nameof(twilioAccountDetails));
        }

        private readonly TwilioAccountDetails _twilioAccountDetails;
        
        public void SendMessage()
        {
          //  add test code here which is listed below
        }
    }
}
```

- This is some test code to add to your SendMessage() method.  This is for testing purposese only so you'll need to figure out how to interact with this with a Razor page, model and appropriate view page to confirm message was sent.  You'll need to change the phone numbers as well.  Use the from: phone # you got from twilio, and your cell # in the to: phone number. 

```csharp
        TwilioClient.Init(_twilioAccountDetails.AccountSid, _twilioAccountDetails.AuthToken);

        var message = MessageResource.Create(
            body: "This is the ship that made the Kessel Run in fourteen parsecs?",
            from: new Twilio.Types.PhoneNumber("8061234567"),
            to: new Twilio.Types.PhoneNumber("8067654321")
        );
```




## Using secret keys
```
// adding the secret manager tool
dotnet add package Microsoft.Extensions.SecretManager.Tools
dotnet add package Microsoft.Extensions.Configuration.UserSecrets

// setting your twilio account details
dotnet user-secrets set TwilioAccountDetails:AccountSid SidYouGotWhenYouSignedUp
dotnet user-secrets set TwilioAccountDetails:AuthToken AuthTokenYouGotWhenYourSignedUp

```

```json
//  remove the hard coded values from your appsettings.json object
"ConnectionStrings": {
    "DefaultConnection": "Default Connection Details",
    "ApplicationDbContextConnection": "DataSource=YourProject.db"
  },
  "TwilioAccountDetails": {
    "AccountSid": "Type reminder where your credentials are",
    "AuthToken": "Type reminder where your credentials are"
  },
```




### Overwriting AppSettings on Azure

> If you’re deploying a web app to Azure, it’s relatively straightforward to add your secrets, requiring only a single step.  From within the portal, select your web app and then scroll down the menu until you find application settings.
Then enter your secrets, matching case, as key/value pairs. -- 

![Overwriting AppSettings on Azure](./images/Screenshot2.jpg)


- References

[How to send sms messages using csharp](https://www.twilio.com/docs/sms/tutorials/how-to-send-sms-messages-csharp#create-a-new-project-with-the-net-cli)

[User Secrets in a net core web app](https://www.twilio.com/blog/2018/05/user-secrets-in-a-net-core-web-app.html)

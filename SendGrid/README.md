# SendGrid Integration
* **NOTE: This is only to get your email service working.  I did not use an interface here for simplicity**


After setting up your [SendGrid](https://sendgrid.com/) account you will need to do the following:

### Click on your name and select Setup Guide

![Setup Guide](./images/ScreenShot1.jpg)


### Click Start in the "Integrate using our Web API or SMTP Relay" section

![Start](./images/ScreenShot2.jpg)


### Click Choose in the Web API box

![Choose](./images/ScreenShot3.jpg)


### Click Choose C#
![Choose C#](./images/ScreenShot4.jpg)


## Enter a name for your API key, then click Create

![Name and Create API Key](./images/ScreenShot5.jpg)


**Copy your key and paste it somewhere, i.e. notepad(windows) or notes(mac), so you can access it later.**


## Scroll down the page skip the "Create Environment variables" Step

## Navigate to the project folder where you will create your Emailer class, and run the following command to add the SendGrid package.  You'll also need the Microsoft.Extensions.Options package as well and I've provided the command to run as well.

```
dotnet add package SendGrid

dotnet add package Microsoft.Extensions.Options

dotnet restore
```


## Class for your key
Create a class for your SendGrid key
```csharp
public class SendGridAccountDetails
{
    public string SGApiKey { get; set; }
}
```



## Update your appsettings.json
Add a key/value pair that stores your SendGrid API Key

```javascript
    "SendGridAccountDetails": {
    "SGApiKey": "Set in Secrets"
    }

```


## Storing your SG Key in user secrets

Navigate the folder of your main mvc app project before running these commands.  **HINT: it's where your Startup.cs resides"

```
dotnet add package Microsoft.Extensions.Configuration.UserSecrets

dotnet add package Microsoft.Extensions.SecretManager.Tools

dotnet restore
```

### Let's add the key to your user-secrets

```
dotnet user-secrets set SendGridAccountDetails:SGApiKey <YourKeyFromSendGrid>

```


## Set services in Startup.cs
This is where you will make your key accessible as the app starts up.

Add the following code within the ConfigureServices method

```csharp

services.Configure<SendGridAccountDetails>(Configuration.GetSection("SendGridAccountDetails"));

```


## SendGrid Email Class
Create a class for your Email Service and put the following code in it.

```csharp
using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using SendGrid;
using SendGrid.Helpers.Mail;
using Microsoft.Extensions.Configuration;

namespace YourNameSpace  // change this to the namespace in your project
{
    public class SendGridEmailer  // name this whatever makes sense for you
    {   
        // the constructor name should match the class name you chose.
        public SendGridEmailer(IOptions<SendGridAccountDetails> optionsAccessor)
        {
            _SendGrid = optionsAccessor.Value;
        }

        private readonly SendGridAccountDetails _SendGrid;//set only via Secret Manager

        // name this method whatever you think makes sense.
        public void SendEmail()  
        {
            Execute().Wait();
        }

        async Task Execute()
        {
            var apiKey = _SendGrid.SGApiKey; // this will get your key from your secrets.json
            var client = new SendGridClient(apiKey);
            var from = new EmailAddress("test@example.com", "Example User");
            var subject = "Sending with SendGrid is Fun";
            var to = new EmailAddress("test@example.com", "Example User");
            var plainTextContent = "and easy to do anywhere, even with C#";
            var htmlContent = "<strong>and easy to do anywhere, even with C#</strong>";
            var msg = MailHelper.CreateSingleEmail(from, to, subject, plainTextContent, htmlContent);
            var response = await client.SendEmailAsync(msg);
        }
    }
}
```



## Message Controller

```csharp
using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace PRPC
{
    public class MessageController : Controller
    {
        // Make sure this name matches your class that you created 
        private readonly SendGridEmailer _emailMessage;

                                // This injects the Class that you created and stores it in the private variable _emailMessage
                                // AKA Dependency Injection.   A better way would be to inject an Interface here but that's for 
                                                              // another time
        public MessageController(SendGridEmailer emailMessage)
        {
            _emailMessage = emailMessage;
        }

        public void Email()
        {
            // this calls the SendMessage method from the class you created for sending the message
            _emailMessage.SendMessage();
        }

    }
}

```



## To test to see if your app sends and email, when the app loads after running dotnet run, then add Message/Email to your url and press enter.


If there were no errors, then you will go back to the SendGrid page and scroll down to bottom of the page and check the box "I've integrated the code above" and click the Next:Verify Integration button

![Integration button](./images/ScreenShot6.jpg)


### Click Verify Integration

![Verify Integration](./images/ScreenShot7.jpg)


### You should now see the message It Worked
![It Worked](./images/ScreenShot8.jpg)







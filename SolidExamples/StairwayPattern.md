## Implementing the Stariway Pattern


![Stairway Pattern](./images/StairwayPattern.png)



I found the question I had about understanding how a client can depend only on an interface in an assembly as well as the service only depending on the interface assembly.  What I quickly realized is that, without referencing the service classlib assembly, there's no way to register the concrete class that fulfills the implementation details of the interface.  This answer came from a question posted on [StackOverflow](https://stackoverflow.com/questions/29259414/stairway-pattern-implementation).  The cool thing about this was, it was the author of the Adaptive Code book, Gary McLean Hall as quoted below.


> "Application entry points should be the composition root, as per Mark Seemann's excellent book on Dependency Injection. Here, the issue is more about Dependency Inversion, whereby both client and implementation should both depend on abstractions.
What that diagram doesn't show, but hopefully other parts of the book make clear, is that the entry point will naturally and necessarily reference everything that is needed to construct whatever are your resolution roots (controllers, services, etc.) But this is the only place that has such knowledge.
Bear in mind that it is sometimes ok for clients to 'own' the interfaces on which they depend: the interface ISecurityService might live in the Controllers assembly, the IUserRepository might live in the ServiceImplementations assembly, and so on. This is impractical when >1 client needs access to the interface, of course.
If you follow SOLID, you will naturally find that Dependency Injection is a necessity, but Inversion of Control containers are less of a priority. I find myself using Pure Dependency Injection (manual construction of resolution roots) more and more often".


He goes on to clarify to answer follow up questions.

> "If I understand you question correctly, then the answer is no. The core API should reference interfaces and not implementations. It is up to the composition root (which is almost always the project containing the entry point of the application) to supply the implementation to the client via some form of dependency injection. I'm writing the 2nd edition at the moment and it will be out in the new year."


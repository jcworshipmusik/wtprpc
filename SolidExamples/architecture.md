# Technical Architecture Requirements

* MVC and/or Razor pages startup project

* Documentation
    - Using Markdown files i.e. README.md

* Code is written in separate class libraries
    * Interfaces
    * Concrete implementation
    * [Creating Library Projects and Solutions in Visual Studio Code : Dr. Babb](https://www.youtube.com/watch?v=0SVf0_s-96E)

* Solid Principles (Links are to the videos provided by Dr. Babb)

    * [Single Responsibility](https://www.youtube.com/watch?v=Zvnhcduw4m0)
        - A class should have one, and only one, reason to change.
    * [Open/Closed](https://www.youtube.com/watch?v=I6txMPEktR8)
        - "Software entities ... should be open for extension, but closed for modification."
    * [Liskov Subsitution](https://www.youtube.com/watch?v=l68kFo4ZNNQ)
        - "Objects in a program should be replaceable with instances of their subtypes without altering the correctness of that program."
    * [Interface segregation](https://www.youtube.com/watch?v=txaSXjMcJtY)
        - "Many client-specific interfaces are better than one general-purpose interface."
    * [Dependency Inversion](https://www.youtube.com/watch?v=9376P1F5HkE)
        - One should "depend upon abstractions, not concretions."

* Identity
    * Customizable App User class with appropriate properties

* Database
    * Repository Pattern
    * Development
        * Sql lite
    * Production
        * SQL Server
            - Dr. Babb will provide us with his SQL Server connection string

* Dependency Injection

* Project deployed to Azure

* SendGrid Email Service

* Twillio Text Service

* Private Git Repo
    * Bitbucket
    * Multiple branches

* Ridership card
    * Simulate reading a card

* Payment system
    - Interface IPaymentProvider
    - Concrete
        - StripeProvider

* xUnit Test project
    * Wednesday March 27 in-class workshop with 1 voluntold member from each team

--- 
## Features
* ReCaptcha








## reCAPTCHA Resource Links

[reCAPTCHA Developers Guide](https://developers.google.com/recaptcha/intro)

[How to add reCAPTCHA to your .NET Core MVC project](https://retifrav.github.io/blog/2017/08/23/dotnet-core-mvc-recaptcha/)
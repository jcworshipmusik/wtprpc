# Modeling Real World Objects

It’s easy to get lost in the complexity of the MVC architecture and forget one basic thing, and that is, Classes are blue prints and can be used to model real world [objects](#/ "noun: person, place, or thing").  Attributes are used to define information about the object, i.e. name, color, make, vin, etc.  Methods are actions that on object can do, Go(), Stop(), Turn(), etc.   This sprint you’ll be working on ridership cards.   The best way to relate to a ridership card is to consider your Buff ID card.   How can you create a model of that in a class?   Basically all your card has on it, whether swiping, or tapping is a unique number, in this case your Buff ID.   This card doesn’t perform any actions, it just stores a unique ID via a magnetic strip or using Near Field Communication (NFC) technology.    This can be a simple class containing 1 maybe 2 properties, the Unique ID and possibly a Name.   

```csharp
    public class Card
    {
        public string CardId { get; set; }

        public string Name { get: set: }
    }
```

The next real world object to consider is a card reader.   Consider the ones that you swipe in with.   The most fundamental function of a card reader is to read cards.  

```csharp

    public class CardReader
    {
        // The card reader takes a card, reads the Id, and returns it to the caller
        public string ReadCard(Card card)
        {
            return Card.Id;
        }
    }

```

One last thing to consider is the person, which has a card to ride the bus with, and to interact with a card reader.

```csharp

    public class AppUser
    {
        public int Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public Card Card { get; set; }
    }

```

I wanted to put this out there to help you all and get you to thinking about how you can use classes to model real world objects, how to define them, and how as an actor, they would interact with your applicaiton.
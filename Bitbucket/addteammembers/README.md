# Adding team members to your repo

Before you begin this section you should have already created a private Bitbucket repo and gone through the steps of adding a devel branch.  If not, go back [here](https://bitbucket.org/jcworshipmusik/wtprpc/src/master/Bitbucket/gitworkflow/)

### 1) Settings / User access
![Screen Shot 1.jpg](../images/ScreenShot1.jpg)

### 2) Enter a team member username
![Screen Shot 2.jpg](../images/ScreenShot2.jpg)

![Screen Shot 3.jpg](../images/ScreenShot3.jpg)

### 3) Grant Write access
![Screen Shot 4.jpg](../images/ScreenShot4.jpg)

### 4) Repeat steps 2 and 3 for each team member

### 5) Team members will receive an email with a link to the repo.
- Have them follow the follwing instructions.

---

# This following section is for the team members who were added to the private Bitbucket repo

## 1) Click the link to be re-directed to the private repo.
![Screen Shot 5](../images/ScreenShot5.jpg)

# Cloning the repo

## 2) Look for the git clone command that you'll need to run
    * Copy this command and run it in the folder that you want have your repo
![Screen Shot 6](../images/ScreenShot6.jpg)

# Checking out devel branch

### 3) cd into the folder that was cloned.
 
### 4) running this command will create your devel branch and take you into it
![Screen Shot 7](../images/ScreenShot7.jpg)

### 5) Running this command will pull down the files in the devel branch from the bitbucket repo
![Screen Shot 8](../images/ScreenShot8.jpg)

### 6) Devel branch is where you will push and pull from as a team.

Next Read about [Git Etiquette](https://bitbucket.org/jcworshipmusik/wtprpc/src/master/Bitbucket/gitetiquette)

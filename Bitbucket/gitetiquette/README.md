## Git Etiquette

I highly suggest that teams assign different sections of code to work on so that you avoid merge problems.  

For example:

* Team member 1 works on MapController.cs and Views/Map/Index.cshtml
* Team member 2 works on HomeController.cs and Views/Home/Index.cshtml
* etc...
---
When you sit down to code, the first thing to do is:

1. Run the command ` git pull origin devel ` so that you have the latest code that has been pushed to the devel branch.  
![Screen Shot 8](../images/ScreenShot8.jpg)

2. When are finished coding and have committed your changes.  Let your team mates know that you are pushing your changes.

3. Run the command ` git push -u origin devel `.
![Screen Shot 8](../images/ScreenShot9.jpg)

This branching style will make for a great work flow during your sprints.

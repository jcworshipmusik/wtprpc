# Git Branching Work Flow

This will guide the origniator of the git repository in how to start a braching model that will be well suited for team work.  This will ony need to be done once because there is only one repo that you will add team members to collaborate on.  Once you've created your project and you have initialized your folder to use git tracking by running ` git init `, it's time to make a development branch.  The master branch is not where you will be coding.  The master branch is where you will merge your final changes at the end of each sprint.

### Pushing to your remote repository
You will need to push your master branch to your repository .
    - **NOTE:** If you haven't created a remote connection to your bitbucket repo then now is the time to do that before moving on.

#### add remote connection:
* ` git remote add origin https://<yourusername>@bitbucket.org/<yourusername>/<yourbitbucketreponame>.git `

####push to remote connection:
* ` git push -u origin master `

    - If prompted, enter your username and password for your bitbucket account.

### Creating your development branch
1. In your top level folder where your project resides you will run

* ` git checkout -b devel `.
    - This command creates a new branch called devel and copies all your code from the master branch and puts you in the devel branch.

2. Push your devel branch to your remote bitbucket repo by running

* ` git push -u origin devel `.
    - This command creates the devel branch in your remote repo and pushes your code into it.

That's it.  Now you will work in this devel branch.  Next you'll need read how to add team members to your private repo. I've documented how to do that [here](https://bitbucket.org/jcworshipmusik/wtprpc/src/master/Bitbucket/addteammembers/).
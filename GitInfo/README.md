## Git topics

* .gitignore
 * If you add files or folders to your .gitignore and you are still seeing thoses in your repo, it means that you have commited them at some point and .git has started tracking them.  You will need to run the ` git rm --cached <fileOrFolderName> ` command. Below are few examples:

- Untracking single file
    - git rm --cached fileName.txt

- Untracking single folder (-r is recursive and is used for folder directories)
    - git rm -r --cached bin/

- Untracking muliple folders (-r is recursive and is used for folder directories)
    - git rm -r --cached bin/ obj/ .vscode/

* Branching models

##### Articles
* [A successful Git branching model](https://nvie.com/posts/a-successful-git-branching-model/)
    * [Supplemental video for this article](https://www.youtube.com/watch?v=faJIdpF70xM&list=WL&index=3&t=0s)

##### Video Resources
* [Git Tutorial for Beginners: A Quick Start Guide](https://www.youtube.com/watch?v=ugN-IYV1NTM)

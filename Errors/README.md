## Team Spartan Restore Popup Issue

I was able to locate the issue with your restore pop up window coming up over and over.  In the integrated terminaL, I clicked on OUTPUT and then in the drop down list, I chose OmniSharp Log and found the following error: 
```
[warn]: OmniSharp.MSBuild.PackageDependencyChecker
        RegistrationUserAndLogin.2: Found package reference 'Sendgrid', but none of the versions in the lock file ("1.0.0") satisfy [9.10.0, )
```

The following is how to resolve this issue:

1. Navigate to PRPC/obj and click on project.assets.json
![Project.assests.json1](./images/ScreenShot1.jpg)
2. click in the window where the file contents of project.assests.json are
3. ctrl+f to bring up the search window
4. Type in SendGrid/1.0.0 and press enter
5. You’ll find that there are 2 places where this occurs
![Project.assests.json2](./images/ScreenShot2.jpg)

6. These numbers need to match what you have in your package reference in your RegistrationUserAndLogin.2.csproj which happens to be 9.10.0
![Project.assests.json3](./images/ScreenShot3.jpg)

![RegistrationUserAndLogin.2.csproj](./images/ScreenShot4.jpg)

7. Change SendGrid/1.0.0 to SendGrid/9.10.0 in both locations.
![Project.assests.json4](./images/ScreenShot5.jpg)

8. When you click restore on the popup window, this should resolve your issue with the popup window that keeps coming up.
